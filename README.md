IEDM, an Ontology for Irradiation Experiment Data Management  
  
CERN, MINES ParisTech  
  
IEDM is an OWL-based Irradiation Experiment Data Management ontology.
One of the key design choices for IEDM was to maximize the reuse of existing foundational ontologies such as the Ontology of Scientific Experiments (EXPO), the Ontology of Units of Measure (OM) and the Friend-of-a-Friend Ontology (FOAF).
The description of an actual irradiation experiment recently performed at IRRAD, the CERN proton irradiation facility, is provided. 
Keywords: Ontology, OWL, irradiation experiment, data management, High Energy Physics.